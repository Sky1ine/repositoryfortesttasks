﻿<?php 

// Text описывает структуру текста его основные свойства.
// Содержит закрытые свойства для ограничения доступа к ним и доступные методы для клиентской работы с текстом.


class Text
{
	// основное свойство - длинна всего файла(текста в нем)
	private $full_text_length = null;
	
	// это поле для количества слов в тексте
	private $count_words = null;
	
	// тут ведется подсчёт всех символов без пробелов
	private $count_sybols_without_spaces = null;
	
	// далее идут функции, позволяющие получить основную информацию о тексте
	public function	GetFullTextLength()
	{
		return $this->full_text_length;
	}
	
	public function	GetCountWords()
	{
		return $this->count_words;
	}
	
	public function	GetSymbolsWithoutSpaces()
	{
		return $this->count_sybols_without_spaces;
	}
	
	// Обработчик текста.
	// Во всех исключительных ситуациях можно добавить функцию, записывающую возникающие ошибки в лог на сервер
	public function TextHandler($input_file_name)
	{
		// два уровня защиты от возможных коллизий
		if (!is_string($input_file_name))
			exit("Возникла ошибка при обработке текста. Отсутствует имя файла");
		if(empty($input_file_name))
			exit("Возникла ошибка при обработке текста. Отсутствует имя файла");
		
		// получаем наш файл "в строку"
		$file_text_array = file_get_contents($input_file_name);
		
		// проверка на пустой файл
		if(!empty($file_text_array))
		{
			// ещё один уровень проверки полученной информации
			if($file_text_array != null)
			{
				// подсчитываем кол-во всех символов в тексте
				$this->full_text_length = strlen($file_text_array);
				
				// удаляем пробелы для дальнейшеё обработки
				$file_text_array = explode(" ", $file_text_array);
				
				// считаем слова
				$this->count_words = count(array_keys($file_text_array));
				
				// получаем кол-во символов без пробелов
				$this->count_sybols_without_spaces = strlen(implode($file_text_array));
				
				// обнуляем наш массив с текстом т.к. работа с ним закончилась				
				$file_text_array = null;
			}
		}
		// исключительная ситуация
		else
			exit("Загружаемый файл пустой или не содержит символов. Пожалуйста, проверьте файл ещё раз перед отправкой");
	}// TextHandler
	
	// при вызове устанавливаем значения всех свойств на null
	public function Clear()
	{
		$this->full_text_length = null;
		$this->count_words = null;
		$this->count_sybols_without_spaces = null;
	}
}// Text
?>
