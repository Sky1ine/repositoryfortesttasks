<?php
	session_start();
	$err = $_FILES['file']['error'];
	if(isset($_FILES['file']['name']))
	{
		// ошибки загрузки файла
		$php_errors = array(1 => 'Превышен максимальный размер файла (.ini)',
							2 => 'Превышен максимальный размер файла в (.html)',
							3 => 'Отправлена только часть файла',
							4 => 'Файл для отправки не был выбран');
		if($err != 0)
			handle_error("сервер не может получить файл.", $php_errors[$err]);
		else
		{
			if(!is_uploaded_file($_FILES['file']['tmp_name']))
				handle_error("Попытка загрузить файл ".$_FILES['file']['name']." не удалась.", "Ошибка загрузки файла, проверьте файл перед отправкой на сервер");
			$file_url = "/home/u971923136/public_html/".basename($_FILES['FILE']['name']);
			// переносим файл в файловую систему хостинга
			if (move_uploaded_file($_FILES['FILE']['tmp_name'], $file_url))
			{ 
				// запоминаем текущее расположение для дальнейшей обработки
				$_SESSION['file_tmp'] = $file_url;
				// ссылаемся на скрипт, выполняющий обработку текста
				header("location: input.php");
			} 
		}
		
	}
	
	// функция, отлавливающая ошибки при загрузке файла
	function handle_error($user_error_message, $system_error_message)
	{
		header("location: show_error.php?"."error_message={$system_error_message}&"."system_error_message={$user_error_message}");
		exit();
	}
?>
