﻿<?php
	session_start();
?>

<html>
    <head>
		<title>joboffer.webdev	</title>
		<meta charset="utf-8"></meta>
    </head>
    <body>
		<h3>Информация о файле: </h3><br \>
		<table>
			<tr>
				<th><span>Имя файла</span></th>
				<th><span>Длинна текста в файле</span></th>
				<th><span>Количество слов</span></th>
				<th><span>Количество символов без пробелов</span></th>
			</tr>
			<tr style="text-align:center">
				<td><?php echo $_SESSION['filename']?></td>
				<td><?php echo $_REQUEST['tlength']?></td>
				<td><?php echo $_REQUEST['wcount']?></td>
				<td><?php echo $_REQUEST['swos']?></td>
			</tr>
		</table>
		<br \>
		<a href="index.php">Вернуться</a>
    </body>
</html>