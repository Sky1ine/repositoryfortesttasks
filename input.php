<?php
		session_start();
		// подключаем наш класс, описывающий свойства в тексте документа
		require_once('text_counter.php');
		$text = new Text();
		// записываем полный путь к расположению файла при загрузке на сервер
		$file_name = $_SESSION['file_tmp'];
		
		// передаем путь к файлу обработчику текста
		$text->TextHandler($file_name);
		
		// получаем основную информацию о тексте
		$file_text_length = $text->GetFullTextLength();
		$file_words_count = $text->GetCountWords();
		$file_symbols_without_spaces = $text->GetSymbolsWithoutSpaces();
		
		// очищаем все значения свойств, так как все необходимые данные были получены.
		$text->Clear();
		// удаляем файл из сервера, так как работа с ним была закончена.
		unlink($file_name);
		
		// отсылаем всю информацию о тексте методом GET на страницу, которая выведет её пользователю
		header("location: infofile.php?"."tlength={$file_text_length}&"."wcount={$file_words_count}&"."swos={$file_symbols_without_spaces}");
?>

